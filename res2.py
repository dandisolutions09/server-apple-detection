import cv2

# Load the image
image = cv2.imread('/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.jpg')

# Load the foreground mask
fgMask = cv2.imread('/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.png', cv2.IMREAD_GRAYSCALE)

# Apply the foreground mask to the image
result = cv2.bitwise_and(image, image, mask=fgMask)

# Save the resulting image
cv2.imwrite('result_with_mask.jpg', result)

# Show the original image and the resulting image with mask applied
cv2.imshow('Image', image)
cv2.imshow('Result with Mask', result)

# Wait for a key press and close all windows
cv2.waitKey(0)
cv2.destroyAllWindows()