# # import torch
# import torchvision.models as models

# # # Load pre-trained ResNet
# resnet = models.resnet50(pretrained=True)

# # # Modify ResNet to output the features needed by YOLOv8
# modules = list(resnet.children())[:-2]  # Remove the fully connected layers
# resnet_backbone = torch.nn.Sequential(*modules)




# # class YOLOv8WithResNet(torch.nn.Module):
# #     def _init_(self):
# #         super(YOLOv8WithResNet, self)._init_()
# #         self.backbone = resnet_backbone
# #         # Define the rest of the YOLOv8 architecture using the feature maps from ResNet

# #     def forward(self, x):
# #         x = self.backbone(x)
# #         # Continue with the rest of YOLOv8's forward pass
# #         return x

# # model = YOLOv8WithResNet()


# # print("model", model)

# # # Assuming you have a DataLoader, loss function, and optimizer set up
# # for epoch in range(num_epochs):
# #     for images, targets in dataloader:
# #         optimizer.zero_grad()
# #         outputs = model(images)
# #         loss = loss_function(outputs, targets)
# #         loss.backward()
# #         optimizer.step()


# import torch
# import torch.nn as nn
# import torch.optim as optim
# from torchvision import datasets, transforms
# from torch.utils.data import DataLoader

# # Define transformations for your dataset
# transform = transforms.Compose([
#     transforms.Resize((224, 224)),
#     transforms.ToTensor(),
#     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
# ])

# # Load your dataset
# dataset = datasets.ImageFolder(root='path/to/your/data', transform=transform)

# # Create a DataLoader
# dataloader = DataLoader(dataset, batch_size=32, shuffle=True, num_workers=4)

# # Define a simple model for testing
# class YOLOv8WithResNet(nn.Module):
#     def __init__(self):
#         super(YOLOv8WithResNet, self).__init__()
#         self.backbone = resnet_backbone
#         self.conv1 = nn.Conv2d(2048, 1024, kernel_size=1)
#         self.conv2 = nn.Conv2d(1024, 512, kernel_size=3, padding=1)
#         self.conv3 = nn.Conv2d(512, 256, kernel_size=1)
#         self.conv4 = nn.Conv2d(256, 128, kernel_size=3, padding=1)
#         self.conv5 = nn.Conv2d(128, 64, kernel_size=1)
#         self.conv6 = nn.Conv2d(64, 30, kernel_size=1)

#     def forward(self, x):
#         x = self.backbone(x)
#         x = self.conv1(x)
#         x = self.conv2(x)
#         x = self.conv3(x)
#         x = self.conv4(x)
#         x = self.conv5(x)
#         x = self.conv6(x)
#         return x

# model = YOLOv8WithResNet()

# # Define a simple loss function and optimizer for testing
# loss_function = nn.MSELoss()
# optimizer = optim.Adam(model.parameters(), lr=0.001)

# # Training loop
# num_epochs = 10
# for epoch in range(num_epochs):
#     for images, targets in dataloader:
#         optimizer.zero_grad()
#         outputs = model(images)
#         loss = loss_function(outputs, targets)
#         loss.backward()
#         optimizer.step()





import os
import yaml
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from PIL import Image
import torchvision.transforms as transforms
import torchvision.models as models

# Load data.yaml
with open('data.yaml') as f:
    data_config = yaml.safe_load(f)

# Extract paths and class names
train_images_dir = data_config['train']
val_images_dir = data_config['val']
class_names = data_config['names']
num_classes = data_config['nc']

# Custom Dataset Class for YOLO Format
class YOLODataset(Dataset):
    def __init__(self, images_dir, transform=None):
        self.images_dir = images_dir
        self.labels_dir = images_dir.replace('images', 'labels')
        self.transform = transform
        self.image_files = [f for f in os.listdir(images_dir) if f.endswith('.jpg')]
    
    def __len__(self):
        return len(self.image_files)
    
    def __getitem__(self, idx):
        image_file = self.image_files[idx]
        image_path = os.path.join(self.images_dir, image_file)
        label_path = os.path.join(self.labels_dir, image_file.replace('.jpg', '.txt'))

        image = Image.open(image_path).convert('RGB')
        
        boxes = []
        labels = []

        with open(label_path) as f:
            for line in f:
                parts = line.strip().split()
                label = int(parts[0])
                x_center = float(parts[1])
                y_center = float(parts[2])
                width = float(parts[3])
                height = float(parts[4])
                
                x_min = x_center - width / 2
                y_min = y_center - height / 2
                x_max = x_center + width / 2
                y_max = y_center + height / 2
                
                boxes.append([x_min, y_min, x_max, y_max])
                labels.append(label)
        
        boxes = torch.tensor(boxes, dtype=torch.float32)
        labels = torch.tensor(labels, dtype=torch.int64)

        if self.transform:
            image = self.transform(image)
        
        target = {'boxes': boxes, 'labels': labels}
        
        return image, target

# Define transformations for your dataset
transform = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

# Create datasets and dataloaders
train_dataset = YOLODataset(images_dir=train_images_dir, transform=transform)
val_dataset = YOLODataset(images_dir=val_images_dir, transform=transform)

train_dataloader = DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=4)
val_dataloader = DataLoader(val_dataset, batch_size=32, shuffle=False, num_workers=4)

# Define the Model
class YOLOv8WithResNet(nn.Module):
    def __init__(self):
        super(YOLOv8WithResNet, self).__init__()
        resnet = models.resnet50(pretrained=True)
        modules = list(resnet.children())[:-2]  # Remove the fully connected layers
        self.backbone = nn.Sequential(*modules)
        self.conv1 = nn.Conv2d(2048, 1024, kernel_size=1)
        self.conv2 = nn.Conv2d(1024, 512, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(512, 256, kernel_size=1)
        self.conv4 = nn.Conv2d(256, 128, kernel_size=3, padding=1)
        self.conv5 = nn.Conv2d(128, 64, kernel_size=1)
        self.conv6 = nn.Conv2d(64, num_classes * 5, kernel_size=1)  # Adjust output channels for YOLO

    def forward(self, x):
        x = self.backbone(x)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.conv6(x)
        return x

model = YOLOv8WithResNet()

# Define a placeholder loss function and optimizer (replace with suitable loss for object detection)
loss_function = nn.MSELoss()  # Placeholder
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Training loop
num_epochs = 10
for epoch in range(num_epochs):
    model.train()
    for images, targets in train_dataloader:
        optimizer.zero_grad()
        outputs = model(images)
        
        # Compute the loss (this is a placeholder; you should implement the proper loss calculation for YOLO)
        loss = loss_function(outputs, targets['boxes'])  # Adjust this to your loss function
        
        loss.backward()
        optimizer.step()
    
    model.eval()
    with torch.no_grad():
        val_loss = 0
        for images, targets in val_dataloader:
            outputs = model(images)
            val_loss += loss_function(outputs, targets['boxes']).item()  # Adjust to your loss function
        print(f"Epoch {epoch+1}/{num_epochs}, Validation Loss: {val_loss/len(val_dataloader)}")
