from typing import Union
from fastapi.responses import FileResponse
from pathlib import Path


import os
import cv2
from ultralytics import YOLO


#from fastapi import FastAPI


from fastapi import FastAPI, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()


origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:5173",
    "*"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

UPLOAD_DIR = Path("uploaded_files")
PROCESSED_DIR =Path("processed")



@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


# @app.post("/upload/")
# async def upload_picture(file: UploadFile = File(...)):
#     with open(f"uploaded_files/{file.filename}", "wb") as buffer:
#         buffer.write(await file.read())
        
#         #image_path = "/home/don/fiftyone/segmentation/predict/weight/apple_on_tree.jpg.webp"  # Path to input image
#         #output_path = "variation-mix-1.jpg"  # Path to save output image
#         output_path="/home/don/fiftyone/segmentation/server-apple-detection/processed" + file.filename
#         model_path = "/home/don/fiftyone/segmentation/server-apple-detection/model/more_classes_model.pt"  # Path to YOLO model
#         image_path= "/home/don/fiftyone/segmentation/server/uploaded_files/"+ file.filename

#     detect_objects(image_path, output_path, model_path)
#     #return {"filename": file.filename}
#     file_path = PROCESSED_DIR / detect_objects(image_path, output_path, model_path)
#     print("file_path", file_path)
#     if not file_path.is_file():
#         return {"error": "File not found"}
#     return FileResponse(file_path)





# Define relative paths
UPLOAD_DIR = "uploaded_files"
PROCESSED_DIR = "processed"
MODEL_DIR = "model"

# Ensure directories exist
os.makedirs(UPLOAD_DIR, exist_ok=True)
os.makedirs(PROCESSED_DIR, exist_ok=True)

@app.post("/upload/")
async def upload_picture(file: UploadFile = File(...)):
    file_location = os.path.join(UPLOAD_DIR, file.filename)
    with open(file_location, "wb") as buffer:
        buffer.write(await file.read())

    image_path = file_location
    output_path = os.path.join(PROCESSED_DIR, file.filename)
    model_path = os.path.join(MODEL_DIR, "more_classes_model.pt")
 
    url=detect_objects(image_path, output_path, model_path)
    
    full_url="https://server-apple-detection.onrender.com/view/"
    
    if not os.path.isfile(output_path):
        return {"error": "File not found"}
    #return FileResponse(output_path)
    return  {"image_path": full_url+""+file.filename}

@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    #return {"filename": file.filename}
    print("file", file.filename)

@app.get("/view/{filename}")
async def view_uploaded_image(filename: str):
    
    print("filename", filename)
    
    # Return the uploaded image file as a response
    file_path = PROCESSED_DIR+"/"+filename
    
    print("full filepath", file_path)
    # if not file_path.is_file():
    #     return {"error": "File not found"}
    return FileResponse(file_path)






# def detect_objects(image_path, output_path, model_path, threshold=0.5):
#     # Load image
#     image = cv2.imread(image_path)
#     if image is None:
#         print("Error: Could not read the image.")
#         return

#     # Load model
#     model = YOLO(model_path)

#     # Perform object detection
#     results = model(image)[0]
    
#     #print(results)

#     # Draw bounding boxes
#     for result in results.boxes.data.tolist():
#         x1, y1, x2, y2, score, class_id = result
#         if score > threshold:
#             cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 4)
#             cv2.putText(image, results.names[int(class_id)].upper(), (int(x1), int(y1 - 10)),
#                         cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 255, 0), 3, cv2.LINE_AA)

#     # Save output image
#     cv2.imwrite(output_path, image)
#     print("Output image saved at:", output_path)
#     return output_path


def detect_objects(image_path, output_path, model_path, threshold=0.5):
    # Load image
    image = cv2.imread(image_path)
    if image is None:
        print("Error: Could not read the image.")
        return

    # Load model
    model = YOLO(model_path)

    # Perform object detection
    results = model(image)[0]

    # Draw bounding boxes
    for result in results.boxes.data.tolist():
        x1, y1, x2, y2, score, class_id = result
        if score > threshold:
            cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 4)
            # Use lower() to convert the class name to lowercase
            cv2.putText(image, results.names[int(class_id)].lower(), (int(x1), int(y1 - 10)),
                        cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 255, 0), 3, cv2.LINE_AA)

    # Save output image
    cv2.imwrite(output_path, image)
    print("Output image saved at:", output_path)
    return output_path