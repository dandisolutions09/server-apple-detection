import torch
import torch.nn as nn
import torchvision.models as models
import numpy as np

from torchvision import transforms
from PIL import Image
import numpy as np




# def extract_segmented_region(image, mask):
#     # Ensure the mask is binary
#     mask = mask.astype(bool)
    
#     # Get the bounding box of the segmented region
#     coords = np.column_stack(np.where(mask))
#     y_min, x_min = coords.min(axis=0)
#     y_max, x_max = coords.max(axis=0)

#     # Crop the segmented region
#     segmented_region = image.crop((x_min, y_min, x_max, y_max))

#     return segmented_region
def extract_segmented_region(image, mask):
    # Ensure the mask is binary
    mask = mask.astype(bool)

    # Get the bounding box of the segmented region
    coords = np.column_stack(np.where(mask))
    y_min, x_min = coords.min(axis=0)
    y_max, x_max = coords.max(axis=0)

    # Crop the segmented region
    segmented_region = image.crop((x_min, y_min, x_max, y_max))

    return segmented_region

# Load an image and mask
image = Image.open("/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.jpg")
mask = np.array(Image.open("/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.png"))

# Extract the segmented region
segmented_region = extract_segmented_region(image, mask)

# print("segmented", segmented_region)

# Load a pretrained ResNet model
resnet = models.resnet50(pretrained=True)