
import torch
import torch.nn as nn
import torchvision.models as models
import numpy as np
import cv2
from torchvision import transforms


def apply_mask_to_image(image_path, mask_path, output_path):
    # Load the image
    image = cv2.imread(image_path)

    # Load the foreground mask
    fgMask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

    # Apply the foreground mask to the image
    result = cv2.bitwise_and(image, image, mask=fgMask)

    # Save the resulting image
    cv2.imwrite(output_path, result)

    # Show the original image and the resulting image with mask applied
    # cv2.imshow('Image', image)
    # cv2.imshow('Result with Mask', result)

    # # Wait for a key press and close all windows
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

# Load an image and mask
image_path = '/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.jpg'
mask_path = '/home/don/fiftyone/segmentation/server-apple-detection/segmented-images-res/0a8a276ebeace90d.png'
output_path = "features.jpg"

apply_mask_to_image(image_path, mask_path, output_path)


# Load the saved image
image_features = cv2.imread(output_path)

# Convert the image to a tensor and normalize
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])


# Load a pretrained ResNet model
resnet = models.resnet50(pretrained=True)

image_features = transform(image_features).unsqueeze(0)

# Load a pretrained ResNet model
resnet = models.resnet50(pretrained=True)

# Set the model to evaluation mode
resnet.eval()

# Forward pass
with torch.no_grad():
    features = resnet(image_features)

# Print the features
print(features)
